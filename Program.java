public class Program {
    public static void main(String[] args) {
        Report1 r1 = new Report1();
        Report2 r2 = new Report2();
        Report3 r3 = new Report3();
        Report4 r4 = new Report4();

        System.out.println("---------------------------------------");
        System.out.println("--    AUTOSCOUT24 LISTING REPORT     --");
        System.out.println("---------------------------------------");

        r1.getReport();
        
        System.out.println("\n---------------------------------------");
        r2.getReport();
        
        System.out.println("\n---------------------------------------");
        r3.getReport();

        System.out.println("\n---------------------------------------");
        r4.getReport();

        
        System.out.println("---------------------------------------");
        System.out.println("--           END OF REPORT           --");
        System.out.println("---------------------------------------");
    }
}
