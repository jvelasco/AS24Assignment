import readers.ListingsReader;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import models.Listing;

public class Report2 {
    public void getReport() {
        ArrayList<Listing> listings = ListingsReader.readListings();
        int listSize = listings.size();

        // Call the sorting function after tallying the frequency of a make
        Iterator<Entry<String, Integer>> sortedCountOfMake = getSortedCountOfMake(tallyFrequency(listings));

        System.out.println("\nPercentual distribution of available cars by Make");
        System.out.println("Make\t\tDistribution");

        // For all the gathered makes
        while(sortedCountOfMake.hasNext()){
            Entry<String, Integer> entry = sortedCountOfMake.next();
            double percentage = ((double) entry.getValue()/listSize) * 100; // Get the percentage share of the make
            System.out.println(entry.getKey() + "\t\t" + new DecimalFormat("#.00").format(percentage) + "%"); // Print the make and percentage share
        }
    }

    private HashMap<String, Integer> tallyFrequency(ArrayList<Listing> listings){
        HashMap<String, Integer> countOfMake = new HashMap<>(); // Used to tally the frequency of a car make

        // For all the lists,
        for (Listing listing : listings) {
			String makeName = listing.getMake();

            // If the make currently doesn't exist in the tally, add it and start counting at 1
            if (!countOfMake.containsKey(makeName)){
                countOfMake.put(makeName, 1);
            } else {
                // Increment the counter associated with the make
                countOfMake.compute(makeName, (key, value)->value + 1);
            }
        }

        return countOfMake;
    }

    private Iterator<Entry<String, Integer>> getSortedCountOfMake(HashMap<String, Integer> map){
        LinkedHashMap<String, Integer> result = new LinkedHashMap<>();

        // Sorts the passed HashMap in descending order and returns the result as a new HashMap
        result = map.entrySet()
            .stream()
            .sorted(Entry.comparingByValue(Comparator.reverseOrder())) 
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return result.entrySet().iterator();
    }
}
