import readers.ListingsReader;

import java.util.ArrayList;

import models.Listing;

public class Report1 {
    public void getReport() {
        ArrayList<Listing> listings = ListingsReader.readListings();

        // Create a specific ArrayList for each listing type
        ArrayList<Listing> privateListings = new ArrayList<>();
        ArrayList<Listing> dealerListings = new ArrayList<>();
        ArrayList<Listing> otherListings = new ArrayList<>();

        // Add each specific listing to the type where they belong
        for (Listing listing : listings) {
            switch(listing.getSellerType()){
                case PRIVATE: privateListings.add(listing); break;
                case DEALER: dealerListings.add(listing); break;
                default: otherListings.add(listing); break;
            }
        }

        System.out.println("\nAverage Listing Selling Price per Seller Type");
        System.out.println("Seller Type \t Average in Euro");
        System.out.println("Private \t € " + getAverageSellingPrice(privateListings)+ ",-");
        System.out.println("Dealer \t\t € " + getAverageSellingPrice(dealerListings) + ",-");
        System.out.println("Other \t\t € " + getAverageSellingPrice(otherListings) + ",-");
    }

    public int getAverageSellingPrice(ArrayList<Listing> listings){
        int result = 0;

        for (Listing listing : listings){
            result += listing.getPrice();
        }

        return result / listings.size();
    }
}
