import readers.ContactFrequencyReader;
import readers.ListingsReader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import models.Listing;

public class Report3 {
    public void getReport(){
        // ArrayList<Listing> listings = ListingsReader.readListings();
        HashMap<Integer, HashSet<Date>> contacts = ContactFrequencyReader.readContacts();

        // Call the sorting function
        LinkedHashMap<Integer, HashSet<Date>> sortedContacts = getSortedCountOfContacts(contacts);

        // Get the 30% most contacted listings
        ArrayList<Entry<Integer, HashSet<Date>>> topThirtyPercent = getTopThirty(sortedContacts);

        // Compute for the average selling price
        int averageSellingPrice = computeAveragePrice(topThirtyPercent);

        System.out.println("\nAverage price");
        System.out.println("€ " + averageSellingPrice + ",-");
    }

    private int computeAveragePrice(ArrayList<Entry<Integer, HashSet<Date>>> topThirtyListings){
        ArrayList<Listing> listings = ListingsReader.readListings();
        int result = 0;

        for (Entry<Integer, HashSet<Date>> contact : topThirtyListings){
            int i;

            for (i = 0; i<listings.size() && contact.getKey()!=listings.get(i).getId(); i++) {}

            result += listings.get(i).getPrice();
        }

        return result / topThirtyListings.size();
    }

    private ArrayList<Entry<Integer, HashSet<Date>>> getTopThirty(LinkedHashMap<Integer, HashSet<Date>> map){
        int i = 0;
        int thirtyPercent = (int) Math.floor(map.size() * 0.3);
        ArrayList<Entry<Integer, HashSet<Date>>> result = new ArrayList<>();
        Iterator<Entry<Integer, HashSet<Date>>> transformedSet = map.entrySet().iterator();

        while (i<thirtyPercent && transformedSet.hasNext()){
            result.add(transformedSet.next());
            i++;
        }

        return result;
    }

    private LinkedHashMap<Integer, HashSet<Date>> getSortedCountOfContacts(HashMap<Integer, HashSet<Date>> map){
        LinkedHashMap<Integer, HashSet<Date>> result = new LinkedHashMap<>();

        Comparator<HashSet<Date>> hashSetComparator = new Comparator<HashSet<Date>>()
        {
            @Override
            public int compare(HashSet<Date> o1, HashSet<Date> o2)
            {
                return Integer.compare(o2.size(), o1.size());
            }
        };

        // Sorts the passed HashMap in descending order and returns the result as a new HashMap
        result = map.entrySet()
            .stream()
            .sorted(Entry.comparingByValue(hashSetComparator)) 
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return result;
    }
}
