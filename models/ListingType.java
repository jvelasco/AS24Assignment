package models;

public enum ListingType {
    PRIVATE,
    DEALER,
    OTHER
}
