package models;

public class Listing {
    private int id;
    private String make;
    private int price;
    private int mileage;
    private ListingType sellerType;

    public Listing(int id, String make, int price, int mileage, String sellerType){
        this.id = id;
        this.make = make;
        this.price = price;
        this.mileage = mileage;
        this.sellerType = ListingType.valueOf(sellerType.toUpperCase());
    }

    public int getId(){
        return id;
    }

    public String getMake(){
        return make;
    }

    public int getPrice(){
        return price;
    }

    public int getMileage(){
        return mileage;
    }

    public ListingType getSellerType(){
        return sellerType;
    }
}
