import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import models.Listing;
import readers.ContactFrequencyReader;
import readers.ListingsReader;

public class Report4 {
    private HashMap<Integer, HashMap<Integer, Map<Integer, Integer>>> contactsDirectory = new HashMap<>();

    public void getReport(){
        populateContactsDirectory();

        sortContactsDirectory();

        showTopFive();
    }

    private void showTopFive(){
        ArrayList<Listing> listings = ListingsReader.readListings();

        for (Entry<Integer, HashMap<Integer, Map<Integer, Integer>>> yearDepth : contactsDirectory.entrySet()) {
            for (Entry<Integer, Map<Integer, Integer>> monthDepth : yearDepth.getValue().entrySet()) {

                int i = 0;
                String month = String.format("%02d", monthDepth.getKey()+1);
                System.out.println("\nTop 5 of Month " + month + "." + yearDepth.getKey());
                System.out.println("Ranking \t| Listing ID \t| Make \t\t\t| Selling Price \t| Mileage \t| Total Amount of Contacts");

                Iterator<Entry<Integer, Integer>> entries = monthDepth.getValue().entrySet().iterator();

                while (i < 5 && entries.hasNext()){
                    int j = 0;
                    i++;

                    Entry<Integer, Integer> entry = entries.next();

                    for (j = 0; j<listings.size() && entry.getKey()!=listings.get(j).getId(); j++) {}

                    Listing listing = listings.get(j);

                    System.out.print(i + " \t\t| " + entry.getKey() + " \t\t| " + listing.getMake());

                    if (listing.getMake().length() < 6) {
                        System.out.print("\t\t\t| € ");
                    } else {
                        System.out.print("\t\t| € ");
                    }

                    System.out.print(listing.getPrice() + ",- \t\t| " + listing.getMileage() + " KM \t| " + entry.getValue() + "\n");                    
                }
            }
        }
    }

    private void sortContactsDirectory(){
        LinkedHashMap<Integer, Integer> result = new LinkedHashMap<>();

        // Convert Hashmap to a set for iterator
        for (Entry<Integer, HashMap<Integer, Map<Integer, Integer>>> yearDepth : contactsDirectory.entrySet()) {
            for (Entry<Integer, Map<Integer, Integer>> monthDepth : yearDepth.getValue().entrySet()) {

                result = monthDepth.getValue().entrySet()
                .stream()
                .sorted(Entry.comparingByValue(Comparator.reverseOrder())) 
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

                monthDepth.setValue(result);
            }
        }
    }

    private void populateContactsDirectory(){
        Iterator<Entry<Integer, HashSet<Date>>> contacts = ContactFrequencyReader.readContacts().entrySet().iterator();

        while(contacts.hasNext()){
            Entry<Integer, HashSet<Date>> entry = contacts.next();

            for (Date date : entry.getValue()){
                addCount(entry.getKey(), date);
            }
        }
    }

    private void addCount(int listingID, Date contactDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(contactDate);
        
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);

        if (!contactsDirectory.containsKey(year)){
            contactsDirectory.put(year, new HashMap<>());
        }

        if (!contactsDirectory.get(year).containsKey(month)){
            contactsDirectory.get(year).put(month, new HashMap<>());
        }

        if (!contactsDirectory.get(year).get(month).containsKey(listingID)){
            contactsDirectory.get(year).get(month).put(listingID, 0);
        }

        contactsDirectory.get(year).get(month).compute(listingID, (key, value)->value + 1);
    }
}
