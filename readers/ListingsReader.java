package readers;

import models.Listing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ListingsReader {
    public static ArrayList<Listing> readListings(){
        String temp = "";
        ArrayList<Listing> AS24Listing = new ArrayList<>();

        BufferedReader br = new BufferedReader(
                new InputStreamReader(ListingsReader.class.getResourceAsStream("listings.csv")));

        try {
            // Move the pointer to the next line
            br.readLine();

            // Populate the listing
            while((temp = br.readLine()) != null){
                String[] row = temp.split(",");

                // While populating, transform the data
                AS24Listing.add(
                    new Listing(Integer.parseInt(row[0]), 
                    row[1].replaceAll("\"", ""), 
                    Integer.parseInt(row[2]),
                    Integer.parseInt(row[3]),
                    row[4].replaceAll("\"", "")));
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
            
        return AS24Listing;
    }
}
