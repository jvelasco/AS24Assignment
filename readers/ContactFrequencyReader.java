package readers;

import java.util.HashMap;
import java.util.HashSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/*
    Stores the contacts.csv contents into a hashmap with the
    listing ID as a key and all contact dates as a value
*/
public class ContactFrequencyReader {
    public static HashMap<Integer, HashSet<Date>> readContacts(){
        HashMap<Integer, HashSet<Date>> contacts = new HashMap<>();

        String temp = "";

        BufferedReader br = new BufferedReader(
                new InputStreamReader(ContactFrequencyReader.class.getResourceAsStream("contacts.csv")));

        try {
            // Move the pointer to the next line
            br.readLine();

            // Populate the listing
            while((temp = br.readLine()) != null){
                String[] row = temp.split(",");

                Integer listingID = Integer.parseInt(row[0]);
                Date contactDate = new Date(Long.parseLong(row[1]));

                // If the listing has never been contacted
                if (!contacts.containsKey(listingID)){
                    contacts.put(listingID, new HashSet<Date>()); // Make space for contact dates
                }

                contacts.get(listingID).add(contactDate); // Add the contact date to its list
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
            
        return contacts;
    }
}
